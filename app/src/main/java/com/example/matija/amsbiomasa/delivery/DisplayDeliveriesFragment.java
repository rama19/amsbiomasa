package com.example.matija.amsbiomasa.delivery;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.print.PrintDocumentInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;

import java.text.SimpleDateFormat;
import java.util.List;


public class DisplayDeliveriesFragment extends Fragment {

    private ListView mainListView;
    private BaseAdapter listAdapter;
    String deliveryId;

    public DisplayDeliveriesFragment() {
    }

    public static DisplayDeliveriesFragment newInstance() {
        return new DisplayDeliveriesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Lista doprema");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.main_listview, container, false);

        mainListView = (ListView) view.findViewById(R.id.ListViewItem);
        final List<Deliveries> deliveriesList = new Select().from(Deliveries.class).orderBy("CreatedAt DESC").execute();
        listAdapter = new DeliveriesAdapter(deliveriesList);
        mainListView.setAdapter(listAdapter);

        //sending RecepitId via intent form DisplayDeliveries Fragment
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deliveryId = String.valueOf(deliveriesList.get(position).getId());
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                DisplayDeliveriesLogsFragment key = new DisplayDeliveriesLogsFragment();

                Bundle bundle = new Bundle();
                bundle.putString("deliveryID", deliveryId);
                key.setArguments(bundle);
                transaction.replace(R.id.fragment_holder, key);

                transaction.addToBackStack(null).commit();
            }
        });

        mainListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deliveryId = String.valueOf(deliveriesList.get(position).getId());
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                UpdateDelivery key = new UpdateDelivery();
                Bundle bundle = new Bundle();
                bundle.putString("deliveryID", deliveryId);
                key.setArguments(bundle);
                transaction.replace(R.id.fragment_holder, key);
                transaction.addToBackStack(null).commit();
                return false;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Obriši dopreme!");
                alert.setMessage("Želite li obrisati sve dopreme?");
                alert.setPositiveButton("DA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Delete().from(Deliveries.class).execute();
                        Intent intent = new Intent(getContext(), DisplayDeliveriesFragment.class);
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("NE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            }
        });

        return view;
    }

    private class DeliveriesAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<Deliveries> deliveriesList;

        public DeliveriesAdapter(List<Deliveries> deliveriesList) {
            inflater = LayoutInflater.from(getContext());
            this.deliveriesList = deliveriesList;
        }

        @Override
        public int getCount() {
            return deliveriesList.size();
        }

        @Override
        public Object getItem(int position) {
            return deliveriesList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return deliveriesList.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.display_deliveries_fragment, parent, false);
            }

            Deliveries deliveries = deliveriesList.get(position);
            ((TextView) convertView.findViewById(R.id.rShipNumb)).setText(deliveries.shippingNumber);
            ((TextView) convertView.findViewById(R.id.rEmployee)).setText(deliveries.employee);
            ((TextView) convertView.findViewById(R.id.rWarehouse)).setText(deliveries.status);
            ((TextView) convertView.findViewById(R.id.rCarrier)).setText(deliveries.carrier);
            ((TextView) convertView.findViewById(R.id.rLicence)).setText(deliveries.licencePlate);
            ((TextView) convertView.findViewById(R.id.rDriver)).setText(deliveries.driver);
            ((TextView) convertView.findViewById(R.id.rCustomer)).setText(deliveries.customer);
            ((TextView) convertView.findViewById(R.id.rDestWareh)).setText(deliveries.destinationWarehouse);
            ((TextView) convertView.findViewById(R.id.rSupplier)).setText(deliveries.supplier);
            ((TextView) convertView.findViewById(R.id.priceType)).setText(deliveries.priceType);
            ((TextView) convertView.findViewById(R.id.rCreatedAt)).setText(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(deliveries.createdAt));

          /*  if (deliveries.priceCorrection > 0 && deliveries.priceCorrection2 > 0) {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText(String.valueOf(deliveries.priceCorrection));
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText(String.valueOf(deliveries.priceCorrection2));
            } else if (deliveries.priceCorrection > 0) {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText(String.valueOf(deliveries.priceCorrection));
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText("-");
            } else if(deliveries.priceCorrection2 > 0) {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText("-");
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText(String.valueOf(deliveries.priceCorrection2));

            }else {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText("-");
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText("-");
            }*/

            return convertView;
        }
    }
}
