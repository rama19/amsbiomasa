package com.example.matija.amsbiomasa.rest;

import com.example.matija.amsbiomasa.pojo.Logs;
import com.example.matija.amsbiomasa.pojo.User;
import com.example.matija.amsbiomasa.pojo.Driver;
import com.example.matija.amsbiomasa.pojo.Partner;
import com.example.matija.amsbiomasa.pojo.Warehouse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface APIService {

        @GET("/api/partners.json")
        Call<List<Partner>> getPartners();

        @GET("/api/drivers.json")
        Call<List<Driver>> getDrivers();

        @GET("/api/warehouses.json")
        Call<List<Warehouse>> getWarehuses();

        @GET("/api/users.json")
        Call<List<User>> getUsers();

        @GET("/api/logs.json")
        Call<List<Logs>> getLogs();
}
