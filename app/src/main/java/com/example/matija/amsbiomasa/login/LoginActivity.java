package com.example.matija.amsbiomasa.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.matija.amsbiomasa.MainActivity;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.User;

public class LoginActivity extends AppCompatActivity implements OnClickListener {

    private EditText userName;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = (EditText) findViewById(R.id.username);
        loginBtn = (Button) findViewById(R.id.buttonLogin);

        loginBtn.setOnClickListener(this);
        setTitle("AMS - biomasa | prijava");


    }

    public void insert() {
        User user = new User();
        user.username=userName.getText().toString();
        user.save();
        Log.d("User", String.valueOf(userName));
        Toast.makeText(getBaseContext(), "Prijavljeni ste!", Toast.LENGTH_SHORT).show();

        SharedPreferences pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor edt = pref.edit();
        edt.putBoolean("activity_executed", true);
        edt.commit();

    }

    @Override
    public void onClick(View v) {
        if (v == loginBtn && validateUser()) {
            insert();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    public Boolean validateUser() {
        if(userName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Unesite ime i prezime", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
