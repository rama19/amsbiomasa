package com.example.matija.amsbiomasa.Handovers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.delivery.AddLogsFragment;
import com.example.matija.amsbiomasa.models.Handover;
import com.example.matija.amsbiomasa.models.User;

import java.util.Date;


public class StartHandover extends Fragment {

    private Button btnSave, btnAddLogs;
    private EditText employee;
    private AutoCompleteTextView customer, warehouse;

    public StartHandover() {}

    public static StartHandover newInstance() { return new StartHandover(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("A.M.S. Biomasa | Započni preuzimanje");

        View view = inflater.inflate(R.layout.start_handover, container, false);

        employee = (EditText) view.findViewById(R.id.worker);
        employee.setText(String.valueOf(new Select().from(User.class).execute()));
        customer = (AutoCompleteTextView) view.findViewById(R.id.customer);
        warehouse = (AutoCompleteTextView) view.findViewById(R.id.warehouse);

        String [] destWarehouses = getResources().getStringArray(R.array.destinationWarehouse_array);
        ArrayAdapter<String> desWrh_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, destWarehouses);
        warehouse.setAdapter(desWrh_adapter);

        btnSave = (Button) view.findViewById(R.id.saveRecepit);
        btnAddLogs = (Button) view.findViewById(R.id.addLogs);

        btnSave.setOnClickListener(btnListener);
        btnAddLogs.setOnClickListener(btnListener);

        return view;
    }

    public void insert() {
        Handover handover = new Handover();
        handover.warehouse = warehouse.getText().toString();
        handover.employee = employee.getText().toString();
        handover.customer = customer.getText().toString();
        handover.createdAt = new Date();
        handover.save();
        Toast.makeText(getContext(), "Spremljeno!", Toast.LENGTH_SHORT).show();
    }
    private Boolean validationSuccess() {
        if (employee.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Polje otpremio je prazno!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (warehouse.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite skladište!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (customer.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite kupca!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    Button.OnClickListener btnListener = (new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment;
            if(v == btnSave && validationSuccess()) {
                insert();
            }
            switch (v.getId()) {
                case R.id.addLogs:
                    fragment = new AddLogsToHandover();
                    replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
