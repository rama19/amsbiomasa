package com.example.matija.amsbiomasa.delivery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;
import com.example.matija.amsbiomasa.models.User;

import java.util.Date;


public class AddDeliveryFragment extends Fragment {

    private EditText shipping_number, employee;
    private AutoCompleteTextView carrier, licence_plate, driver, dest_warehouse, supplier, customer;
    private Button btnSave, btnAddLogs;
    private Spinner priceType, status;

    public AddDeliveryFragment() {
    }

    public static AddDeliveryFragment newInstance() {
        return new AddDeliveryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Započni dopremu");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_delivery, container, false);

        shipping_number = (EditText) view.findViewById(R.id.shippingNumber);
        carrier = (AutoCompleteTextView) view.findViewById(R.id.carrier);
        licence_plate = (AutoCompleteTextView) view.findViewById(R.id.licencePlate);
        driver = (AutoCompleteTextView) view.findViewById(R.id.driver);
        customer = (AutoCompleteTextView) view.findViewById(R.id.customer);
        dest_warehouse = (AutoCompleteTextView) view.findViewById(R.id.destinationWarehouse);
        employee = (EditText) view.findViewById(R.id.worker);
        employee.setText(String.valueOf(new Select().from(User.class).execute()));
        priceType = (Spinner) view.findViewById(R.id.spinnerPrice);
        supplier = (AutoCompleteTextView) view.findViewById(R.id.supplier);
        status = (Spinner) view.findViewById(R.id.status);

        String [] carriers = getResources().getStringArray(R.array.carrier_array);
        ArrayAdapter<String> car_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, carriers);
        carrier.setAdapter(car_adapter);

        String [] lic_plates = getResources().getStringArray(R.array.licencePlate_array);
        ArrayAdapter<String> lic_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, lic_plates);
        licence_plate.setAdapter(lic_adapter);

        String [] drivers = getResources().getStringArray(R.array.driver_array);
        ArrayAdapter<String> dr_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, drivers);
        driver.setAdapter(dr_adapter);

        String [] destWarehouses = getResources().getStringArray(R.array.destinationWarehouse_array);
        ArrayAdapter<String> desWrh_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, destWarehouses);
        dest_warehouse.setAdapter(desWrh_adapter);

        btnSave = (Button) view.findViewById(R.id.saveRecepit);
        btnAddLogs = (Button) view.findViewById(R.id.addLogs);

        btnSave.setOnClickListener(btnListener);
        btnAddLogs.setOnClickListener(btnListener);

        return view;
    }

    public void insert() {
        Deliveries delivery = new Deliveries();
        delivery.shippingNumber=shipping_number.getText().toString();
        delivery.carrier=carrier.getText().toString();
        delivery.licencePlate=licence_plate.getText().toString();
        delivery.driver=driver.getText().toString();
        delivery.customer=customer.getText().toString();
        delivery.destinationWarehouse=dest_warehouse.getText().toString();
        delivery.employee=employee.getText().toString();
        delivery.priceType=priceType.getSelectedItem().toString();
        delivery.supplier = supplier.getText().toString();
        delivery.status = status.getSelectedItem().toString();
        delivery.createdAt=new Date();
        delivery.save();
        Toast.makeText(getContext(), "Spremljeno!", Toast.LENGTH_SHORT).show();
    }

    private Boolean validationSuccess() {
        if(shipping_number.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite broj popratnice!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(carrier.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite prijevoznika!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(licence_plate.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite broj registarske pločice!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(driver.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite ime vozača!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(customer.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite kupca!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(dest_warehouse.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite odredišno skladište!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(employee.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Polje otpremio je prazno!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    Button.OnClickListener btnListener = (new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment;
            if(v == btnSave && validationSuccess()) {
                insert();
            }
            switch (v.getId()) {
                case R.id.addLogs:
                fragment = new AddLogsFragment();
                replaceFragment(fragment);
                break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
