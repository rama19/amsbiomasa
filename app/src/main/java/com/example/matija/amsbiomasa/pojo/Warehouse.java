package com.example.matija.amsbiomasa.pojo;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;


public class Warehouse {

    @Expose
    private List<String> warehouse_name = new ArrayList<>();

    public List<String> getWarehouse_name() { return warehouse_name; }

    public void setWarehouse_name(List<String> warehouse_name) { this.warehouse_name = warehouse_name; }
}
