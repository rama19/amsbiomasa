package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;


@Table(name = "Price")
public class Price extends Model {

    @Column(name = "Sort")
    public
    String sort;

    @Column(name = "Grade")
    public
    String grade;

    @Column(name = "Diameter")
    double diameter;

    @Column(name = "StumpPriceKn")
    public
    double stumpPrice_kn;

    @Column(name = "RoadPriceKn")
    public
    double roadPrice_kn;

    public Price() {}

}
