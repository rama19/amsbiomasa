package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;
import java.util.List;

@Table(name = "Shippings")
public class Shippings extends Model {

    @Column(name = "Employee")
    public String employee;

    @Column(name = "Supplier")
    public String supplier;

    @Column(name = "LoadingPlace")
    public String loadingPlace;

    @Column(name="Carrier")
    public String carrier;

    @Column(name = "Driver")
    public String driver;

    @Column(name="LicencePlate")
    public String licencePlate;

    @Column(name = "Customer")
    public String customer;

    @Column(name= "DestinationWarehouse")
    public String destinationWarehouse;

    @Column(name = "CreatedAt")
    public Date createdAt;

    //public List<Shipped_logs> getLogs() { return getMany(Shipped_logs.class, "Shippings"); }

    public Shippings() {}

    public Shippings(String employee, String supplier,
                     String loadingPlace, String carrier, String driver, String licencePlate, String customer,
                     String destinationWarehouse, Date createdAt) {
        this.employee = employee;
        this.supplier = supplier;
        this.loadingPlace = loadingPlace;
        this.carrier = carrier;
        this.driver = driver;
        this.licencePlate = licencePlate;
        this.customer = customer;
        this.destinationWarehouse = destinationWarehouse;
        this.createdAt = createdAt;
    }



}
