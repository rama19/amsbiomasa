package com.example.matija.amsbiomasa.shipping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.MainActivity;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;
import com.example.matija.amsbiomasa.models.Shippings;

import java.text.SimpleDateFormat;
import java.util.List;


public class DisplayShippings extends Fragment {

    private ListView mainListView;
    private BaseAdapter listAdapter;
    String shippingId;

    public DisplayShippings() {
    }

    public static DisplayShippings newInstance() {
        return new DisplayShippings();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Lista otprema");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.main_listview, container, false);

        mainListView = (ListView) view.findViewById(R.id.ListViewItem);
        final List<Shippings> shippingsList = new Select().from(Shippings.class).orderBy("CreatedAt DESC").execute();
        listAdapter = new ShippingsAdapter(shippingsList);
        mainListView.setAdapter(listAdapter);

        //sending RecepitId via intent form DisplayDeliveries Fragment
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                shippingId = String.valueOf(shippingsList.get(position).getId());
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                DisplayShippedLogs key = new DisplayShippedLogs();

                Bundle bundle = new Bundle();
                bundle.putString("shippingID", shippingId);
                key.setArguments(bundle);
                transaction.replace(R.id.fragment_holder, key);

                transaction.addToBackStack(null).commit();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Obriši otpreme!");
                alert.setMessage("Želite li obrisati sve otpreme?");
                alert.setPositiveButton("DA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Delete().from(Shippings.class).execute();
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("NE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            }
        });

        return view;
    }

    private class ShippingsAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<Shippings> shippingsList;

        public ShippingsAdapter(List<Shippings> shippingsList) {
            inflater = LayoutInflater.from(getContext());
            this.shippingsList = shippingsList;
        }

        @Override
        public int getCount() {
            return shippingsList.size();
        }

        @Override
        public Object getItem(int position) {
            return shippingsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return shippingsList.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.display_shippings, parent, false);
            }

            Shippings shippings = shippingsList.get(position);
            ((TextView) convertView.findViewById(R.id.Supplier)).setText(shippings.supplier);
            ((TextView) convertView.findViewById(R.id.LoadingPlace)).setText(shippings.loadingPlace);
            ((TextView) convertView.findViewById(R.id.Carrier)).setText(shippings.carrier);
            ((TextView) convertView.findViewById(R.id.Driver)).setText(shippings.driver);
            ((TextView) convertView.findViewById(R.id.LicencePlate)).setText(shippings.licencePlate);
            ((TextView) convertView.findViewById(R.id.date)).setText(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(shippings.createdAt));
            ((TextView) convertView.findViewById(R.id.User)).setText(shippings.employee);
            ((TextView) convertView.findViewById(R.id.Customer)).setText(shippings.customer);
            ((TextView) convertView.findViewById(R.id.DischargePlace)).setText(shippings.destinationWarehouse);

          /*  if (shippings.priceCorrection > 0 && shippings.priceCorrection2 > 0) {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText(String.valueOf(shippings.priceCorrection));
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText(String.valueOf(shippings.priceCorrection2));
            } else if (shippings.priceCorrection > 0) {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText(String.valueOf(shippings.priceCorrection));
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText("-");
            } else if(shippings.priceCorrection2 > 0) {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText("-");
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText(String.valueOf(shippings.priceCorrection2));

            }else {
                ((TextView) convertView.findViewById(R.id.rdel_price_percentage)).setText("-");
                ((TextView) convertView.findViewById(R.id.rdel_price_per_m3)).setText("-");
            }*/

            return convertView;
        }
    }
}

