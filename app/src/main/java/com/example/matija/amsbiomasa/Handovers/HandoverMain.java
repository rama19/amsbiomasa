package com.example.matija.amsbiomasa.Handovers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.delivery.AddDeliveryFragment;
import com.example.matija.amsbiomasa.delivery.AddLogsFragment;
import com.example.matija.amsbiomasa.delivery.DisplayDeliveriesFragment;
import com.example.matija.amsbiomasa.delivery.DisplayLogsFragment;


public class HandoverMain extends Fragment {

    Button startHandover, addLogs, allHandovers, allLogs;

    public HandoverMain() {
    }

    public static HandoverMain newInstance() { return new HandoverMain(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("A.M.S. Biomasa | Preuzimanje");

        View view = inflater.inflate(R.layout.delivery_fragment, container, false);

        startHandover = (Button) view.findViewById(R.id.addRecepit);
        addLogs = (Button) view.findViewById(R.id.addLogs);
        allHandovers = (Button) view.findViewById(R.id.allReceipts);
        allLogs = (Button) view.findViewById(R.id.allLogs);
        startHandover.setText("ZAPOČNI PREUZIMANJE");
        addLogs.setText("DODAJ TRUPCE");
        allHandovers.setText("LISTA PREUZIMANJA");
        allLogs.setText("LISTA TRUPACA");
        startHandover.setOnClickListener(btnListener);
        addLogs.setOnClickListener(btnListener);
        allHandovers.setOnClickListener(btnListener);
        allLogs.setOnClickListener(btnListener);
        return view;
    }

    Button.OnClickListener btnListener = (new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            Fragment fragment;
            switch (v.getId()) {
                case R.id.addRecepit:
                    fragment = new StartHandover();
                    replaceFragment(fragment);
                    break;
                case R.id.addLogs:
                    fragment = new AddLogsToHandover();
                    replaceFragment(fragment);
                    break;
                case R.id.allReceipts:
                    fragment = new DisplayDeliveriesFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.allLogs:
                    fragment = new DisplayLogsFragment();
                    replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
