package com.example.matija.amsbiomasa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.matija.amsbiomasa.Handovers.HandoverMain;
import com.example.matija.amsbiomasa.delivery.DeliveryFragment;
import com.example.matija.amsbiomasa.download.DownloadMain;
import com.example.matija.amsbiomasa.measurement.MeasurementMain;
import com.example.matija.amsbiomasa.shipping.ShippingFragment;


public class MainFragment extends Fragment  {


    ImageView shippingFragment, deliveryFragment, measurementFragment, takeOversFragment, download, settings;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, parent, false);
        getActivity().setTitle("A.M.S. Biomasa");

        shippingFragment = (ImageView) view.findViewById(R.id.shipping);
        deliveryFragment = (ImageView) view.findViewById(R.id.delivery);
        measurementFragment = (ImageView) view.findViewById(R.id.meassurement);
        takeOversFragment = (ImageView) view.findViewById(R.id.take_overs);
        download = (ImageView) view.findViewById(R.id.download);
        settings = (ImageView) view.findViewById(R.id.settings);
        shippingFragment.setOnClickListener(imageListener);
        deliveryFragment.setOnClickListener(imageListener);
        measurementFragment.setOnClickListener(imageListener);
        takeOversFragment.setOnClickListener(imageListener);
        download.setOnClickListener(imageListener);
        settings.setOnClickListener(imageListener);

        return view;
    }

    ImageView.OnClickListener imageListener = (new ImageView.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment;
            Intent intent;
            switch (v.getId()) {
                case R.id.delivery:
                    fragment = new DeliveryFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.shipping:
                    fragment = new ShippingFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.meassurement:
                    intent = new Intent(getActivity(), MeasurementMain.class);
                    startActivity(intent);
                    break;
                case R.id.take_overs:
                    fragment = new HandoverMain();
                    replaceFragment(fragment);
                    break;
                case R.id.download:
                    fragment = new DownloadMain();
                    replaceFragment(fragment);
                    break;
               /* case R.id.settings:
                    intent = new Intent(getActivity(), AllLogs.class);
                    startActivity(intent);
                    break;*/
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
