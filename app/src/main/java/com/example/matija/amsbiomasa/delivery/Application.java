package com.example.matija.amsbiomasa.delivery;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.models.Price;
import com.example.matija.amsbiomasa.rest.APIService;


public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);

        boolean priceExists= new Select().from(Price.class).exists();
        if(!priceExists){
            insertPrices();
        }
    }

    private void insertPrices() {

        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < priceSort.length; i++) {
                Price p = new Price();
                p.sort = priceSort[i];
                p.grade = priceGrade[i];
                p.stumpPrice_kn = stumpPriceKN[i];
                p.roadPrice_kn = roadPriceKN[i];
                p.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        }finally {
            ActiveAndroid.endTransaction();
        }
    }

    String priceSort[] = new String[] {"Hrast kitnjak", "Hrast kitnjak", "Hrast kitnjak", "Hrast kitnjak", "Hrast kitnjak", "Jasen", "Jasen", "Jasen", "Jasen",
            "Bagrem", "Bagrem", "Bagrem", "Bagrem", "Bukva", "Bukva", "Bukva", "Bukva", "Grab obicni", "Grab obicni", "Lipa", "Lipa", "Lipa", "Lipa", "Joha", "Joha", "Joha", "Joha" };



    String priceGrade[] = new String[] {"A", "AB", "B", "C", "D", "A", "B", "C", "D", "A", "B", "C", "ST", "A", "B", "C", "D", "C", "D", "A", "B", "C", "D", "A", "B", "C", "D" };
    double stumpPriceKN[] =  { 1900.00, 1300.00, 1100.00, 700.00, 450.00, 1400.00, 900.00, 600.00, 350.00, 500.00, 350.00, 250.00, 220.00, 450.00, 300.00, 200.00, 150.00, 200.00, 150.00, 400.00, 300.00, 200.00, 50.00, 250.00, 180.00, 100.00, 50.00 };
    double roadPriceKN[] = {2000.00, 1400.00, 1200.00, 800.00, 550.00, 1500.00, 1000.00, 700.00, 450.00, 600.00, 450.00, 350.00, 320.00, 550.00, 400.00, 300.00, 250.00, 300.00, 250.00, 500.00, 400.00, 300.00, 150.00, 350.00, 280.00, 200.00, 150.00 };

}