package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table(name = "DeliveryLogs")
public class DeliveryLogs extends Model {

    @Column(name="PlateNumber")
    public
    String plate_number;

    @Column(name="SupplierPlateNumber")
    public
    String supplier_plate_number;

    @Column(name="SortName")
    public
    String sort_name;

    @Column(name="Grade")
    public
    String grade;

    @Column(name = "Diametar")
    public
    double diameter;

    @Column(name="Length")
    public
    double length;

    @Column(name="Mass")
    public
    double mass;

    @Column(name="CreatedAt")
    public
    Date createdAt;

    @Column(name="Deliveries")
    public
    Deliveries deliveries;

    @Column(name = "Price")
    public
    Price price;

    private List<DeliveryLogs> logs_list = new ArrayList<DeliveryLogs>();

    public List<DeliveryLogs> getLogs_list() {
        return logs_list;
    }

    public DeliveryLogs() {
    }

    public DeliveryLogs(String plate_number, String supplier_plate_number, String sort_name, String grade, double diameter, double length, double mass, Date createdAt, Deliveries deliveries) {
        this.plate_number = plate_number;
        this.supplier_plate_number = supplier_plate_number;
        this.sort_name = sort_name;
        this.grade = grade;
        this.diameter = diameter;
        this.length = length;
        this.mass = mass;
        this.createdAt = createdAt;
        this.deliveries = deliveries;
    }

    //Ovo mi je metdoa za računnje mase trupaca.. treba mi za računanje cijene kasnije
  /*  public double getM3() {
        double length, diameter, result;

        length = this.length / 100;
        diameter = (this.diameter * this.diameter) * 3.14159265;
        result = (length * diameter) / 40000;
        return result;
    }*/

}
