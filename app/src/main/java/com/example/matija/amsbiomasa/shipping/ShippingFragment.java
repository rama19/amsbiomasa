package com.example.matija.amsbiomasa.shipping;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.matija.amsbiomasa.R;

public class ShippingFragment extends Fragment {

    Button addShipping, addLogs, showShippings, showLogs;

    public ShippingFragment() {}

    public static ShippingFragment newInstance() { return new ShippingFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. Biomasa | Otprema");

        View view = inflater.inflate(R.layout.shipping_fragment, container, false);

        addShipping = (Button) view.findViewById(R.id.addShipping);
        addLogs = (Button) view.findViewById(R.id.addLogs);
        showShippings = (Button) view.findViewById(R.id.allShippings);
        showLogs = (Button) view.findViewById(R.id.allLogs);
        addShipping.setOnClickListener(btnListener);
        addLogs.setOnClickListener(btnListener);
        showShippings.setOnClickListener(btnListener);
        showLogs.setOnClickListener(btnListener);

        return view;
    }

    Button.OnClickListener btnListener = (new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            Fragment fragment = null;
            switch (v.getId()) {
                case R.id.addShipping:
                    fragment = new AddShippingFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.addLogs:
                    fragment = new AddLogsFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.allShippings:
                    fragment = new DisplayShippings();
                    replaceFragment(fragment);
                    break;
                case R.id.allLogs:
                    fragment = new DisplayShippedLogs();
                    replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
