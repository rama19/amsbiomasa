package com.example.matija.amsbiomasa.delivery;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;
import com.example.matija.amsbiomasa.models.User;

import java.util.List;

public class UpdateDelivery extends Fragment {

    private EditText shipping_number, employee;
    private AutoCompleteTextView carrier, licence_plate, driver, dest_warehouse, supplier, customer;
    private Button btnSave, btnAddLogs;
    private Spinner priceType, status;

    public UpdateDelivery() {}

    public static UpdateDelivery newInstance() {
        return new UpdateDelivery();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("A.M.S. | Uredi dopremu");

        String forwardedId = getArguments().getString("deliveryID");
        //List<Deliveries> delivery = new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute();

        View view = inflater.inflate(R.layout.fragment_add_delivery, container, false);

        shipping_number = (EditText) view.findViewById(R.id.shippingNumber);
        carrier = (AutoCompleteTextView) view.findViewById(R.id.carrier);
        licence_plate = (AutoCompleteTextView) view.findViewById(R.id.licencePlate);
        driver = (AutoCompleteTextView) view.findViewById(R.id.driver);
        customer = (AutoCompleteTextView) view.findViewById(R.id.customer);
        dest_warehouse = (AutoCompleteTextView) view.findViewById(R.id.destinationWarehouse);
        employee = (EditText) view.findViewById(R.id.worker);
        employee.setText(String.valueOf(new Select().from(User.class).execute()));
        priceType = (Spinner) view.findViewById(R.id.spinnerPrice);
        supplier = (AutoCompleteTextView) view.findViewById(R.id.supplier);
        status = (Spinner) view.findViewById(R.id.status);

        shipping_number.setText(String.valueOf(new Select(String.valueOf(shipping_number)).from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        carrier.setText(String.valueOf(new Select(String.valueOf(carrier)).from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        licence_plate.setText(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        driver.setText(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        customer.setText(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        dest_warehouse.setText(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        priceType.setSelected(Boolean.parseBoolean(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute())));
        supplier.setText(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute()));
        status.setSelected(Boolean.parseBoolean(String.valueOf(new Select().from(Deliveries.class).where("Deliveries = " + forwardedId).execute())));

        return view;
    }
}
