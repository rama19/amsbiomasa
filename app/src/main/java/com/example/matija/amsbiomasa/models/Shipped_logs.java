package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="ShippedLogs")
public class Shipped_logs extends Model {

    @Column(name = "PlateNumber")
    public String plateNumber;

    @Column(name = "Shippings")
    public Shippings shippings;


    public Shipped_logs() {}

    public Shipped_logs(String plateNumber, Shippings shippings) {
        this.plateNumber = plateNumber;
        this.shippings = shippings;
    }
}
