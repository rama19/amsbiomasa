package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Partners")
public class Partners extends Model {

    @Column(name = "Name")
    public String name;

    public Partners() {}

    public Partners(String name) {
        this.name = name;
    }
}
