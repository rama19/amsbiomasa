package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

@Table(name = "Handover")
public class Handover extends Model {

    @Column(name = "Employee")
    public String employee;

    @Column(name = "Customer")
    public String customer;

    @Column(name = "Warehouse")
    public String warehouse;

    @Column(name = "CreatedAt")
    public Date createdAt;

    public Handover() {}

    public Handover(String employee, String customer, String warehouse, Date createdAt) {
        this.employee = employee;
        this.customer = customer;
        this.warehouse = warehouse;
        this.createdAt = createdAt;
    }
}
