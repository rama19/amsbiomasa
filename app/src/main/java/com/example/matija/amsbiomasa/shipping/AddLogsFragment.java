package com.example.matija.amsbiomasa.shipping;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;
import com.example.matija.amsbiomasa.models.DeliveryLogs;
import com.example.matija.amsbiomasa.models.Price;
import com.example.matija.amsbiomasa.models.Shipped_logs;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class AddLogsFragment extends Fragment {

    private AutoCompleteTextView plate_number;
    private Button btn_save, btn_logsList;

    public AddLogsFragment() {}

    public static AddLogsFragment newInstance() { return new AddLogsFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Dodaj trupce");

        View view = inflater.inflate(R.layout.add_shipping_logs, container, false);

        plate_number = (AutoCompleteTextView) view.findViewById(R.id.plate_number);

        btn_save = (Button) view.findViewById(R.id.btn_save);
        btn_logsList = (Button) view.findViewById(R.id.btn_logsList);
        btn_save.setOnClickListener(btnListener);
        btn_logsList.setOnClickListener(btnListener);

        return view;
    }

    public void insert() {
        Shipped_logs shippedLogs = new Shipped_logs();
        shippedLogs.plateNumber = plate_number.getText().toString();
        shippedLogs.save();
        Toast.makeText(getContext(), "Spremljeno!", Toast.LENGTH_SHORT).show();
    }

    Button.OnClickListener btnListener = (new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == btn_save && validationSuccess()) {
                insert();
            }
            Fragment fragment;
            switch (v.getId()) {
                case R.id.btn_logsList:
                    fragment = new DisplayShippedLogs()  ;
                    replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private Boolean validationSuccess() {

        if(plate_number.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite broj pločice dobavljača!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}