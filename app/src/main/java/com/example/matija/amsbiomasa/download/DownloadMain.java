package com.example.matija.amsbiomasa.download;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Partners;
import com.example.matija.amsbiomasa.pojo.Driver;
import com.example.matija.amsbiomasa.pojo.Partner;
import com.example.matija.amsbiomasa.rest.APIHelper;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DownloadMain extends Fragment implements Callback<String> {

    private static final String TAG = DownloadMain.class.getSimpleName();

    private Button dloadPartners, takeDrivers, takeWarehouses, takeUsers, takeLogs;
    private Call callPartners, callDrivers, callWarehouses, callUsers, callLogs;

    public DownloadMain() {}

    public DownloadMain newInstance() { return new DownloadMain(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("A.M.S. Biomasa | Download");

        View view = inflater.inflate(R.layout.download_main, container, false);

        dloadPartners = (Button) view.findViewById(R.id.downloadPartners);
        takeDrivers = (Button) view.findViewById(R.id.btnDrivers);
        takeWarehouses = (Button) view.findViewById(R.id.btnDownloadWarehouses);
        takeUsers = (Button) view.findViewById(R.id.btnDownloadUsers);
        takeLogs = (Button) view.findViewById(R.id.btnDownloadLogs);
        dloadPartners.setOnClickListener(btnListener);
        takeDrivers.setOnClickListener(btnDrivers);
        takeWarehouses.setOnClickListener(btnWarehouse);
        takeUsers.setOnClickListener(btnUsers);


        callPartners = APIHelper.getApiService().getPartners();
        callDrivers = APIHelper.getApiService().getDrivers();
        callWarehouses = APIHelper.getApiService().getWarehuses();
        callUsers = APIHelper.getApiService().getUsers();
        callLogs = APIHelper.getApiService().getLogs();

        return view;
    }

    Button.OnClickListener btnListener = (new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            callPartners.clone().enqueue(DownloadMain.this);
            checkIfDataIsInTable();
        }
    });

    Button.OnClickListener btnDrivers = (new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            callDrivers = APIHelper.getApiService().getDrivers();
            callDrivers.enqueue(new Callback<ArrayList<String>>() {

                @Override
                public void onResponse(Call call, Response response) {
                    ArrayList<String> drivers = (ArrayList<String>) response.body();
                    if(response.body() == null) {
                        try {
                            response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), "No Drivers!", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d(TAG, "Number of partners received: " + drivers.size());
                        Toast.makeText(getActivity(), "Drivers downloaded!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        }
    });

    Button.OnClickListener btnWarehouse = (new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            callWarehouses = APIHelper.getApiService().getWarehuses();
            callWarehouses.enqueue(new Callback<ArrayList<String>>() {
                @Override
                public void onResponse(Call call, Response response) {
                    ArrayList<String> warehouses = (ArrayList<String>) response.body();
                    if(response.body() == null) {
                        try {
                            response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), "No warehouses!", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d(TAG, "Number of warehouses received: " + warehouses.size());
                        Toast.makeText(getActivity(), "Warehouses downloaded!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        }
    });

    Button.OnClickListener btnUsers = (new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            callWarehouses.enqueue(new Callback<ArrayList<String>>() {
                @Override
                public void onResponse(Call call, Response response) {
                    ArrayList<String> users = (ArrayList<String>) response.body();
                    if(response.body() == null) {
                        try {
                            response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), "No users!", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d(TAG, "Number of users received: " + users.size());
                        Toast.makeText(getActivity(), "Users downloaded!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        }
    });

    Button.OnClickListener btnLogs = (new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            callLogs.enqueue(new Callback<ArrayList<String>>() {
                @Override
                public void onResponse(Call call, Response response) {
                    ArrayList<String> logs = (ArrayList<String>) response.body();
                    if(response.body() == null) {
                        try {
                            response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), "No logs!", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d(TAG, "Number of logs received: " + logs.size());
                        Toast.makeText(getActivity(), "Logs downloaded!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });
        }
    });

    @Override
    public void onResponse(Call call, Response response) {

        ArrayList<String> partners = (ArrayList<String>) response.body();

        if(response.body() == null) {
            try {
                response.errorBody().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getActivity(), "No Partners!", Toast.LENGTH_SHORT).show();
        } else {
            ActiveAndroid.beginTransaction();
            try {
                for (int i = 0; i < partners.size() ; i++) {
                    Partners partner = new  Partners();
                    partner.name = String.valueOf(partners);
                    partner.save();
                    Log.d("partner_ ", String.valueOf(response.body()));
                }
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }

            Log.d(TAG, "Number of partners received: " + partners.size());
            Toast.makeText(getActivity(), "Partners downloaded!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }

    private void checkIfDataIsInTable() {
        List<Partners> myArrayList = new ArrayList<>();
        myArrayList = new Select().from(Partners.class).execute();
        Log.d("PARTNERI PRISUTNI", String.valueOf(myArrayList.size()));
    }

    private void insertPartners() {
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < 100; i++) {
                Partner partner = new Partner();
                partner.getName();
                //partner.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

}
