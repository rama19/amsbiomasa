package com.example.matija.amsbiomasa.delivery;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.DeliveryLogs;

import java.text.SimpleDateFormat;
import java.util.List;


public class DisplayLogsFragment extends Fragment {

    private ListView mainListView;
    private BaseAdapter listAdapter;

    public DisplayLogsFragment() {
    }

    public static DisplayLogsFragment newInstance() {
        return new DisplayLogsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Lista trupaca");
        View view = inflater.inflate(R.layout.main_listview, container, false);

        mainListView = (ListView) view.findViewById(R.id.ListViewItem);

        final List<DeliveryLogs> logsList = new Select().from(DeliveryLogs.class).orderBy("CreatedAt").execute();
        listAdapter = new LogsArrayAdapter(logsList);
        mainListView.setAdapter(listAdapter);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Obriši trupce!");
                alert.setMessage("Želite li obrisati sve trupce?");
                alert.setPositiveButton("DA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Delete().from(DeliveryLogs.class).execute();
                        Intent intent = new Intent(getContext(), DeliveryLogs.class);
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("NE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            }
        });

        return view;
    }

    private class LogsArrayAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<DeliveryLogs> logsList;

        public LogsArrayAdapter(List<DeliveryLogs> logsList) {
            inflater = LayoutInflater.from(getActivity());
            this.logsList = logsList;
        }

        @Override
        public int getCount() {
            return logsList.size();
        }

        @Override
        public Object getItem(int position) {
            return logsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return logsList.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.display_logs_fragment, parent, false);
            }
            DeliveryLogs log = logsList.get(position);
            ((TextView) convertView.findViewById(R.id.textNumber)).setText(log.plate_number);
            ((TextView) convertView.findViewById(R.id.textSort)).setText(log.sort_name);
            ((TextView) convertView.findViewById(R.id.textGrade)).setText(log.grade);
            ((TextView) convertView.findViewById(R.id.textDiameter)).setText(log.diameter+"cm");
            ((TextView) convertView.findViewById(R.id.textLength)).setText(log.length+"cm");

            ((TextView) convertView.findViewById(R.id.textDate)).setText(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(log.createdAt));
            return convertView;
        }
    }
}
