package com.example.matija.amsbiomasa.shipping;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;
import com.example.matija.amsbiomasa.models.Shippings;
import com.example.matija.amsbiomasa.models.User;

import java.util.Date;


public class AddShippingFragment extends Fragment {

    private EditText employee, priceCorrection, priceCorrection2;
    private AutoCompleteTextView carrier, loading_place, licence_plate, driver, dest_warehouse, supplier, customer;
    private Button btnSave, btnAddLogs;

    public AddShippingFragment() {
    }

    public static AddShippingFragment newInstance() {
        return new AddShippingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Započni otpremu");

        View view = inflater.inflate(R.layout.fragment_add_shipping, container, false);

        employee = (EditText) view.findViewById(R.id.worker);
        employee.setText(String.valueOf(new Select().from(User.class).execute()));
        supplier = (AutoCompleteTextView) view.findViewById(R.id.supplier);
        loading_place = (AutoCompleteTextView) view.findViewById(R.id.loading_place);
        carrier = (AutoCompleteTextView) view.findViewById(R.id.carrier);
        driver = (AutoCompleteTextView) view.findViewById(R.id.driver);
        licence_plate = (AutoCompleteTextView) view.findViewById(R.id.licencePlate);
        customer = (AutoCompleteTextView) view.findViewById(R.id.customer);
        dest_warehouse = (AutoCompleteTextView) view.findViewById(R.id.destinationWarehouse);

        String [] carriers = getResources().getStringArray(R.array.carrier_array);
        ArrayAdapter<String> car_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, carriers);
        carrier.setAdapter(car_adapter);

        String [] lic_plates = getResources().getStringArray(R.array.licencePlate_array);
        ArrayAdapter<String> lic_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, lic_plates);
        licence_plate.setAdapter(lic_adapter);

        String [] drivers = getResources().getStringArray(R.array.driver_array);
        ArrayAdapter<String> dr_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, drivers);
        driver.setAdapter(dr_adapter);

        String [] destWarehouses = getResources().getStringArray(R.array.destinationWarehouse_array);
        ArrayAdapter<String> desWrh_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, destWarehouses);
        dest_warehouse.setAdapter(desWrh_adapter);

        btnSave = (Button) view.findViewById(R.id.saveRecepit);
        btnAddLogs = (Button) view.findViewById(R.id.addLogs);

        btnSave.setOnClickListener(btnListener);
        btnAddLogs.setOnClickListener(btnListener);

        return view;
    }

    public void insert() {
        Shippings shippings = new Shippings();
        shippings.employee=employee.getText().toString();
        shippings.supplier = supplier.getText().toString();
        shippings.loadingPlace = loading_place.getText().toString();
        shippings.carrier=carrier.getText().toString();
        shippings.driver=driver.getText().toString();
        shippings.licencePlate=licence_plate.getText().toString();
        shippings.customer=customer.getText().toString();
        shippings.destinationWarehouse=dest_warehouse.getText().toString();
        shippings.createdAt=new Date();
        shippings.save();
        Toast.makeText(getContext(), "Spremljeno!", Toast.LENGTH_SHORT).show();
    }

    private Boolean validationSuccess() {
        if(loading_place.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite mjesto utovara!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(carrier.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite naziv prijevoznika!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(licence_plate.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite broj registarske pločice!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(driver.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite ime vozača!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(customer.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite kupca!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(dest_warehouse.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite mjesto istovara!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(employee.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Polje otpremio je prazno!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    Button.OnClickListener btnListener = (new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment;
            if(v == btnSave && validationSuccess()) {
                insert();
            }
            switch (v.getId()) {
                case R.id.addLogs:
                fragment = new AddLogsFragment();
                replaceFragment(fragment);
                break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
