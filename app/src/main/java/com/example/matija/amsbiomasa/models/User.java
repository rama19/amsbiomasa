package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by matija on 24/06/16.
 */
@Table(name="User")
public class User extends Model {

    @Column(name="Username")
    public
    String username;

    public User() {}

    @Override
    public String toString() {
        return username;
    }
}
