package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "HandoverLogs")
public class Handover_logs extends Model {

    @Column(name = "PlateNumber")
    public String plateNumber;

    @Column(name = "Handover")
    public Handover handover;

    public Handover_logs() {}

    public Handover_logs(String plateNumber, Handover handover) {
        this.plateNumber = plateNumber;
        this.handover = handover;
    }
}
