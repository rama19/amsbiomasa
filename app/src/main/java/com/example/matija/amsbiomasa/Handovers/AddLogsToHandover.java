package com.example.matija.amsbiomasa.Handovers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Handover_logs;
import com.example.matija.amsbiomasa.shipping.DisplayShippedLogs;


public class AddLogsToHandover extends Fragment {

    private EditText plate_number;
    private Button btn_save, btn_logsList;

    public AddLogsToHandover() {}

    public static AddLogsToHandover newInstance() { return new AddLogsToHandover(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("A.M.S. Biomasa | Dodaj trupac");

        View view = inflater.inflate(R.layout.add_shipping_logs, container, false);

        plate_number = (AutoCompleteTextView) view.findViewById(R.id.plate_number);

        btn_save = (Button) view.findViewById(R.id.btn_save);
        btn_logsList = (Button) view.findViewById(R.id.btn_logsList);
        btn_save.setOnClickListener(btnListener);
        btn_logsList.setOnClickListener(btnListener);

        return view;

    }

    private void insert() {
        Handover_logs handoverLogs = new Handover_logs();
        handoverLogs.plateNumber = plate_number.getText().toString();
    }

    Button.OnClickListener btnListener = (new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == btn_save && validationSuccess()) {
                insert();
            }
            Fragment fragment;
            switch (v.getId()) {
                case R.id.btn_logsList:
                    //fragment = new DisplayShippedLogs()  ;
                    //replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private Boolean validationSuccess() {

        if(plate_number.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite broj pločice!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
