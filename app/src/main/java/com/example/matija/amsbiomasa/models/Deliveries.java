package com.example.matija.amsbiomasa.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;
import java.util.List;

@Table(name="Deliveries")
public class Deliveries extends Model {

    @Column(name="ShippingNumber")
    public
    String shippingNumber;

    @Column(name="Carrier")
    public
    String carrier;

    @Column(name="LicencePlate")
    public
    String licencePlate;

    @Column(name = "Driver")
    public
    String driver;

    @Column(name = "Customer")
    public
    String customer;

    @Column(name= "DestinationWarehouse")
    public
    String destinationWarehouse;

    @Column(name = "Employee")
    public
    String employee;

    @Column(name = "PriceType")
    public
    String priceType;

    @Column(name = "Supplier")
    public
    String supplier;

    @Column(name = "CreatedAt")
    public
    Date createdAt;

    @Column(name = "Status")
    public
    String status;

    public List<DeliveryLogs> getLogs() { return getMany(DeliveryLogs.class, "Deliveries");}

    public Deliveries() {}

    public String typeOfPrice() { return priceType; }

    @Override
    public String toString() {
        return "Broj popratnice: "+shippingNumber;
    }

    public Deliveries(String shippingNumber, String carrier, String licencePlate,
                      String driver, String customer, String destinationWarehouse, String employee, String priceType,
                      String supplier, Date createdAt, String status) {

        this.shippingNumber = shippingNumber;
        this.carrier = carrier;
        this.licencePlate = licencePlate;
        this.driver = driver;
        this.customer = customer;
        this.destinationWarehouse = destinationWarehouse;
        this.employee = employee;
        this.priceType = priceType;
        this.supplier = supplier;
        this.createdAt = createdAt;
        this.status = status;
    }

}
