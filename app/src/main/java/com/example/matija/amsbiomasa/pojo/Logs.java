package com.example.matija.amsbiomasa.pojo;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;


public class Logs {

        @Expose
        private List<String> logs = new ArrayList<>();

        public List<String> getLogs() { return logs; }

        public void setLogs(List<String> logs) { this.logs = logs; }

}
