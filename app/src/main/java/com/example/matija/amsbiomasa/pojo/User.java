package com.example.matija.amsbiomasa.pojo;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class User {

    @Expose
    private List<String> user_name = new ArrayList<>();

    public List<String> getUser_name() { return user_name; }

    public void setUser_name(List<String> user_name) { this.user_name = user_name; }

}
