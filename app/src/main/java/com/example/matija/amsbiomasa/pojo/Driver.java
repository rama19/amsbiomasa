package com.example.matija.amsbiomasa.pojo;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Driver {

    @Expose
    private List<String> driver_name = new ArrayList<String>();

    public List<String> getDriver_name() { return driver_name; }

    public void setDriver_name(List<String> driver_name) { this.driver_name = driver_name; }
}
