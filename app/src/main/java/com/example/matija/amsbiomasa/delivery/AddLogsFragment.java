package com.example.matija.amsbiomasa.delivery;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.Deliveries;
import com.example.matija.amsbiomasa.models.DeliveryLogs;
import com.example.matija.amsbiomasa.models.Price;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class AddLogsFragment extends Fragment {

    private EditText plate_number, supplier_plate, diameterLog, lengthLog;
    private Spinner gradeid,receiptsSprinner, sort_name;
    private Button btn_save, btn_logsList;

    public AddLogsFragment() {}

    public static AddLogsFragment newInstance() { return new AddLogsFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("A.M.S. | Dodaj trupce");

        View view = inflater.inflate(R.layout.fragment_add_logs, container, false);

        plate_number = (EditText) view.findViewById(R.id.plateNumber);
        supplier_plate = (EditText) view.findViewById(R.id.supplierPlateNumber);
        sort_name = (Spinner) view.findViewById(R.id.logs_sort);
        gradeid = (Spinner) view.findViewById(R.id.spinnerClassInput);
        diameterLog = (EditText) view.findViewById(R.id.diameterInput);
        lengthLog = (EditText) view.findViewById(R.id.lengthInput);
        receiptsSprinner=(Spinner) view.findViewById(R.id.spinnerReceipt);

        List<Deliveries> receiptList= new Select().from(Deliveries.class).orderBy("CreatedAt DESC").execute();

        ArrayAdapter<Deliveries> adapter= new ArrayAdapter<Deliveries>(getActivity(), android.R.layout.simple_spinner_item,receiptList);
        receiptsSprinner.setAdapter(adapter);

        btn_save = (Button) view.findViewById(R.id.btn_save);
        btn_logsList = (Button) view.findViewById(R.id.btn_logsList);


        btn_save.setOnClickListener(btnListener);
        btn_logsList.setOnClickListener(btnListener);

        return view;
    }

    public void insert() {
        DeliveryLogs deliveryLogs = new DeliveryLogs();
        deliveryLogs.plate_number = plate_number.getText().toString();
        deliveryLogs.supplier_plate_number = supplier_plate.getText().toString();
        deliveryLogs.sort_name = sort_name.getSelectedItem().toString();
        deliveryLogs.grade = gradeid.getSelectedItem().toString();
        deliveryLogs.diameter = Double.valueOf(diameterLog.getText().toString());
        deliveryLogs.length = Double.valueOf(lengthLog.getText().toString());
        deliveryLogs.mass = getM3();
        deliveryLogs.deliveries = (Deliveries)receiptsSprinner.getSelectedItem();
        deliveryLogs.price = getPrice(deliveryLogs);
        deliveryLogs.createdAt = new Date();
        deliveryLogs.save();
        Toast.makeText(getContext(), "Spremljeno!", Toast.LENGTH_SHORT).show();
    }

    public double getM3() {
        double length, diameter, result;

        length = Double.valueOf(lengthLog.getText().toString()) / 100;
        diameter = (Double.valueOf(diameterLog.getText().toString()) * Double.valueOf(diameterLog.getText().toString())) * 3.14159265;
        result = (length * diameter) / 40000;
        return result;
    }

    public Price getPrice(DeliveryLogs deliveryLogs){
        From select= new Select().from(Price.class).where("Sort LIKE ? AND Grade = ?", deliveryLogs.sort_name, deliveryLogs.grade);
        Log.d("price", select.toSql());
        Price p = select.executeSingle();
        Log.e("grade", p.grade);
        Log.e("sort", p.sort);
        return p;
    }

    Button.OnClickListener btnListener = (new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == btn_save && validationSuccess()) {
                insert();
                BigInteger t = new BigInteger(plate_number.getText().toString());
                plate_number.setText(String.valueOf(t.add(BigInteger.ONE)));
                diameterLog.setText("");
                lengthLog.setText("");
            }
            Fragment fragment;
            switch (v.getId()) {
                case R.id.btn_logsList:
                    fragment = new DisplayDeliveriesLogsFragment()  ;
                    replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private Boolean validationSuccess() {

        if(plate_number.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite broj pločice dobavljača!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(diameterLog.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite promjer trupca!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(lengthLog.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Unesite duljinu trupca!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
