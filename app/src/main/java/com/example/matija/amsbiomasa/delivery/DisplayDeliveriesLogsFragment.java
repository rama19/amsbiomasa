package com.example.matija.amsbiomasa.delivery;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.BundleCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.DeliveryLogs;

import java.text.SimpleDateFormat;
import java.util.List;


public class DisplayDeliveriesLogsFragment extends Fragment {

    private ListView mainListView;
    private BaseAdapter listAdapter;


    public DisplayDeliveriesLogsFragment() {
    }

    public static DisplayDeliveriesLogsFragment newInstance() {
        return new DisplayDeliveriesLogsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String forwardedId = getArguments().getString("deliveryID");
        List<DeliveryLogs> logsList = new Select().from(DeliveryLogs.class).where("Deliveries = " + forwardedId).execute();
        View view = inflater.inflate(R.layout.main_listview, container, false);

        mainListView = (ListView) view.findViewById(R.id.ListViewItem);

        //recieve RecepitID
        //final long forwardedId = (long) new BundleCompat().getExtras().get(String.valueOf("recepitID"));


        listAdapter = new LogsArrayAdapter(logsList);
        mainListView.setAdapter(listAdapter);

        //Long click to delete clicked item
        mainListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                TextView data = (TextView) view.findViewById(R.id.textNumber);
                final String plateNumber = data.getText().toString();
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Obriši trupac!");
                alert.setMessage("Želite li obrisati ovaj trupac?");

                //Deleteing method
                alert.setPositiveButton("OBRIŠI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Delete().from(DeliveryLogs.class).where("PlateNumber = ?", plateNumber).execute();
                        Intent intent = new Intent(getContext(), DisplayDeliveriesLogsFragment.class);
                        startActivity(intent);
                        Toast.makeText(getActivity(), "Trupac sa brojem pločice " + plateNumber + " je obrisan!", Toast.LENGTH_SHORT).show();
                    }
                });
                alert.show();
                return true;
            }
        });

        getActivity().setTitle("A.M.S. | doprema " + forwardedId);
        return view;
    }

    private class LogsArrayAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<DeliveryLogs> logsList;

        public LogsArrayAdapter(List<DeliveryLogs> logsList) {
            inflater = LayoutInflater.from(getContext());
            this.logsList = logsList;
        }

        @Override
        public int getCount() {
            return logsList.size();
        }

        @Override
        public Object getItem(int position) {
            return logsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return logsList.get(position).getId();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.display_deliveries_logs_fragment, parent, false);
            }
            DeliveryLogs log = logsList.get(position);
            ((TextView) convertView.findViewById(R.id.textNumber)).setText(log.plate_number);
            ((TextView) convertView.findViewById(R.id.textSort)).setText(log.sort_name);
            ((TextView) convertView.findViewById(R.id.textGrade)).setText(log.grade);
            ((TextView) convertView.findViewById(R.id.textDiameter)).setText(log.diameter + "cm");
            ((TextView) convertView.findViewById(R.id.textLength)).setText(log.length + "cm");
            Log.d("Value", log.createdAt.toString());
            ((TextView) convertView.findViewById(R.id.textDate)).setText(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(log.createdAt));
            ((TextView) convertView.findViewById(R.id.textAmount)).setText(String.format("%.2f m3", log.mass));
            Log.d("Cijena_panjKN:", String.valueOf(log.price.stumpPrice_kn));
            Log.d("Cijena_cestaKN:", String.valueOf(log.price.roadPrice_kn));
            Log.d("RESULT =>", String.format("%.0f", log.price.stumpPrice_kn * log.mass));
            if (log.deliveries.priceType.equals("Na panju")) {
                if (log.deliveries.priceType.equals("Na panju")) {
                    ((TextView) convertView.findViewById(R.id.priceKN)).setText(String.format("%.2f kn", log.price.stumpPrice_kn * log.mass));
                } else {
                    ((TextView) convertView.findViewById(R.id.priceKN)).setText(String.format("%.2f kn", log.price.roadPrice_kn * log.mass));
                }
            }
            return convertView;
        }
    }
}