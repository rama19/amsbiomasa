package com.example.matija.amsbiomasa.shipping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.example.matija.amsbiomasa.R;
import com.example.matija.amsbiomasa.models.DeliveryLogs;
import com.example.matija.amsbiomasa.models.Shipped_logs;

import java.text.SimpleDateFormat;
import java.util.List;


public class DisplayShippedLogs extends Fragment {

    private ListView mainListView;
    private BaseAdapter listAdapter;

    public DisplayShippedLogs() {
    }

    public static DisplayShippedLogs newInstance() {
        return new DisplayShippedLogs();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String forwardedId = getArguments().getString("shippingID");
        List<Shipped_logs> logsList = new Select().from(Shipped_logs.class).where("Shippings = " + forwardedId).execute();

        View view = inflater.inflate(R.layout.main_listview, container, false);

        mainListView = (ListView) view.findViewById(R.id.ListViewItem);

        listAdapter = new ShippedLogs(logsList);
        mainListView.setAdapter(listAdapter);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Obriši trupce!");
                alert.setMessage("Želite li obrisati sve trupce?");
                alert.setPositiveButton("DA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Delete().from(DeliveryLogs.class).execute();
                        Intent intent = new Intent(getContext(), DeliveryLogs.class);
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("NE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            }
        });
        getActivity().setTitle("A.M.S. | Otprema " + forwardedId);
        return view;
    }

    private class ShippedLogs extends BaseAdapter {
        private LayoutInflater inflater;
        private List<Shipped_logs> logsList;

        public ShippedLogs(List<Shipped_logs> logsList) {
            inflater = LayoutInflater.from(getActivity());
            this.logsList = logsList;
        }

        @Override
        public int getCount() {
            return logsList.size();
        }

        @Override
        public Object getItem(int position) {
            return logsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return logsList.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.display_logs_fragment, parent, false);
            }
            Shipped_logs shipped_logs = logsList.get(position);
            ((TextView) convertView.findViewById(R.id.plateNumber)).setText(shipped_logs.plateNumber);

            return convertView;
        }
    }
}
