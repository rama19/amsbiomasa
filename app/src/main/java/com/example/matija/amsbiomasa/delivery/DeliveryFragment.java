package com.example.matija.amsbiomasa.delivery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.matija.amsbiomasa.R;

public class DeliveryFragment extends Fragment {

    Button addReceipts, addLogs, showReceipts, showLogs;

    public DeliveryFragment() {}

    public static DeliveryFragment newInstance() { return new DeliveryFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("A.M.S. Biomasa | Doprema");

        View view = inflater.inflate(R.layout.delivery_fragment, container, false);

        addReceipts = (Button) view.findViewById(R.id.addRecepit);
        addLogs = (Button) view.findViewById(R.id.addLogs);
        showReceipts = (Button) view.findViewById(R.id.allReceipts);
        showLogs = (Button) view.findViewById(R.id.allLogs);
        addReceipts.setOnClickListener(btnListener);
        addLogs.setOnClickListener(btnListener);
        showReceipts.setOnClickListener(btnListener);
        showLogs.setOnClickListener(btnListener);

        return view;
    }

    Button.OnClickListener btnListener = (new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            Fragment fragment;
            switch (v.getId()) {
                case R.id.addRecepit:
                    fragment = new AddDeliveryFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.addLogs:
                    fragment = new AddLogsFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.allReceipts:
                    fragment = new DisplayDeliveriesFragment();
                    replaceFragment(fragment);
                    break;
                case R.id.allLogs:
                    fragment = new DisplayLogsFragment();
                    replaceFragment(fragment);
                    break;
            }
        }
    });

    public void replaceFragment(android.support.v4.app.Fragment rFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, rFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
